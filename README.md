# Insurance P and C InsurTech

Repository to store Insurance demo environment API versions from the P&C InsurTech Workspace -> Claims Mobile API

***This workspace is part of a set of demo workspaces the Postman Solutions Engineering team put together to show how different industries can utilize workspaces to suit their needs, and does not represent a real company. You can read more about the process of creating this workspace in the companion [blog post](https://blog.postman.com/postman-enterprise-healthcare-insurance-industries-leverage-api-response-data/).***

**Household Policies API**:  
Household Policies is our internal API managing household policy logic that is not covered in the general Claims API.
- Associated OpenAPI spec
- Mock server. 

**ID Verification API**: 
This RubikPay integration verifies a user's ID during our PM customer onboarding process.

**Legacy Claims API**:  
This is our internal API to handle general information related to PM Claims, Accounts, etc.    
- Associated OpenAPI spec.  
- Mock server.  
- Monitor.    
- Visualizations:
    - `Claims` folder > `GET Claims` 
    - `Claims` folder > `GET Claim images`. 
**Mobile Claims API**: 
This is the microservice offshoot from the original legacy Claims API, specifically built to support Claims actions on our mobile apps. Much the same as what you'll find in the legacy system, but smaller, simpler, and easier to maintain. This project is still a work in progress, and pull requests and comments are encouraged. 
- Associated OpenAPI spec.  
- Mock server.  
- Visualizations:
    - `Claims` folder > `GET Claims` 
    - `Claims` folder > `GET Claim images`.

**Pay As You Drive Insurance**: This API is an integration with Mercedes Benz, allowing us to pull odometer info to calculate a mileage-based rate as opposed to our standard billing model.

**Policyholder 360**: Holds all customer dashboard API calls: customer-facing account management, settings, renewal options, view and file claims.
- Associated OpenAPI spec
- Mock server. 


**Quotes API**:  
Ategrity handles specialty coverage that is outside of our domain. See the `Risk Appetite` folder for instructions on setting up coverage for unique situations.   
- Mock server. 

**Vehicle Insurance Service**: Our internal API managing vehicle policy logic that is not covered in the general Claims API.
